<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInit68c64a44027340268cf41c975dcba717
{
    public static $prefixLengthsPsr4 = array (
        'S' => 
        array (
            'Stripe\\' => 7,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'Stripe\\' => 
        array (
            0 => __DIR__ . '/..' . '/stripe/stripe-php/lib',
        ),
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInit68c64a44027340268cf41c975dcba717::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInit68c64a44027340268cf41c975dcba717::$prefixDirsPsr4;

        }, null, ClassLoader::class);
    }
}
